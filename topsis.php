<?php
session_start();
include('koneksi.php');

//Bobot
$data = [4, 1, 2, 2, 1];

$W1    = $data[0];
$W2    = $data[1];
$W3    = $data[2];
$W4    = $data[3];
$W5    = $data[4];

//Pembagi Normalisasi
function pembagiNM($matrik)
{
    for ($i = 0; $i < 5; $i++) {
        $pangkatdua[$i] = 0;
        for ($j = 0; $j < sizeof($matrik); $j++) {
            $pangkatdua[$i] = $pangkatdua[$i] + pow($matrik[$j][$i], 2);
        }
        $pembagi[$i] = sqrt($pangkatdua[$i]);
    }
    return $pembagi;
}

//Normalisasi
function Transpose($squareArray)
{

    if ($squareArray == null) {
        return null;
    }
    $rotatedArray = array();
    $r = 0;

    foreach ($squareArray as $row) {
        $c = 0;
        if (is_array($row)) {
            foreach ($row as $cell) {
                $rotatedArray[$c][$r] = $cell;
                ++$c;
            }
        } else $rotatedArray[$c][$r] = $row;
        ++$r;
    }
    return $rotatedArray;
}

function JarakIplus($aplus, $bob)
{
    for ($i = 0; $i < sizeof($bob); $i++) {
        $dplus[$i] = 0;
        for ($j = 0; $j < sizeof($aplus); $j++) {
            $dplus[$i] = $dplus[$i] + pow(($aplus[$j] - $bob[$i][$j]), 2);
        }
        $dplus[$i] = round(sqrt($dplus[$i]), 4);
    }
    return $dplus;
}


// MULAI PROGRAM
$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$no = 1;
while ($data_laptop = mysqli_fetch_array($query)) {
    $Matrik[$no - 1] = array($data_laptop['harga_angka'], $data_laptop['ram_angka'], $data_laptop['memori_angka'], $data_laptop['processor_angka'], $data_laptop['kamera_angka']);
    $no++;
}

$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$no = 1;
$pembagiNM = pembagiNM($Matrik);
while ($data_laptop = mysqli_fetch_array($query)) {

    $MatrikNormalisasi[$no - 1] = array(
        $data_laptop['harga_angka'] / $pembagiNM[0],
        $data_laptop['ram_angka'] / $pembagiNM[1],
        $data_laptop['memori_angka'] / $pembagiNM[2],
        $data_laptop['processor_angka'] / $pembagiNM[3],
        $data_laptop['kamera_angka'] / $pembagiNM[4]
    );
    $no++;
}

$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$no = 1;
$pembagiNM = pembagiNM($Matrik);
while ($data_laptop = mysqli_fetch_array($query)) {

    $NormalisasiBobot[$no - 1] = array(
        $MatrikNormalisasi[$no - 1][0] * $W1,
        $MatrikNormalisasi[$no - 1][1] * $W2,
        $MatrikNormalisasi[$no - 1][2] * $W3,
        $MatrikNormalisasi[$no - 1][3] * $W4,
        $MatrikNormalisasi[$no - 1][4] * $W5
    );
    $no++;
}

$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$no = 1;
$pembagiNM = pembagiNM($Matrik);
while ($data_laptop = mysqli_fetch_array($query)) {

    $MatrikNormalisasi[$no - 1] = array(
        $data_laptop['harga_angka'] / $pembagiNM[0],
        $data_laptop['ram_angka'] / $pembagiNM[1],
        $data_laptop['memori_angka'] / $pembagiNM[2],
        $data_laptop['processor_angka'] / $pembagiNM[3],
        $data_laptop['kamera_angka'] / $pembagiNM[4]
    );
    $no++;
}

$NormalisasiBobotTrans = Transpose($NormalisasiBobot);

$idealpositif = array(min($NormalisasiBobotTrans[0]), max($NormalisasiBobotTrans[1]), max($NormalisasiBobotTrans[2]), max($NormalisasiBobotTrans[3]), max($NormalisasiBobotTrans[4]));

$idealnegatif = array(max($NormalisasiBobotTrans[0]), min($NormalisasiBobotTrans[1]), min($NormalisasiBobotTrans[2]), min($NormalisasiBobotTrans[3]), min($NormalisasiBobotTrans[4]));

$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$Dplus = JarakIplus($idealpositif, $NormalisasiBobot);
$Dmin = JarakIplus($idealnegatif, $NormalisasiBobot);

$query = mysqli_query($selectdb, "SELECT * FROM data_laptop");
$no = 1;
$nilaiV = array();
while ($data_laptop = mysqli_fetch_array($query)) {

    array_push($nilaiV, $Dmin[$no - 1] / ($Dmin[$no - 1] + $Dplus[$no - 1]));
    $no++;
}

$testmax = max($nilaiV);
for ($i = 0; $i < count($nilaiV); $i++) {
    if ($nilaiV[$i] == $testmax) {
        $query = mysqli_query($selectdb, "SELECT * FROM data_laptop where id_laptop = $i+1");
        while ($user = mysqli_fetch_array($query)) {
            echo $user['nama_laptop'];
        }
    }
}
