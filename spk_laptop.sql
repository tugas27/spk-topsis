/*
 Navicat Premium Data Transfer

 Source Server         : My Computer
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : spk_laptop

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 04/12/2022 15:28:19
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for data_laptop
-- ----------------------------
DROP TABLE IF EXISTS `data_laptop`;
CREATE TABLE `data_laptop`  (
  `id_laptop` int(4) NOT NULL AUTO_INCREMENT,
  `nama_laptop` varchar(256) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `harga_laptop` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ram_laptop` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `memori_laptop` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `processor_laptop` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `kamera_laptop` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `harga_angka` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `ram_angka` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `memori_angka` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `processor_angka` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `kamera_angka` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_laptop`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_laptop
-- ----------------------------
INSERT INTO `data_laptop` VALUES (1, 'MSI 1', '6800000', '4', '256', 'AMD', '8', '4', '1', '3', '3', '2');
INSERT INTO `data_laptop` VALUES (2, 'Lenovo 1', '2200000', '4', '128', 'AMD', '5', '5', '1', '2', '3', '1');
INSERT INTO `data_laptop` VALUES (3, 'Asus 1', '44800000', '24', '2048', 'INTEL', '12', '1', '5', '5', '5', '3');
INSERT INTO `data_laptop` VALUES (4, 'MSI 2', '7596000', '12', '512', 'INTEL', '13', '4', '3', '4', '5', '3');
INSERT INTO `data_laptop` VALUES (5, 'Lenovo 2', '14596000', '16', '2048', 'INTEL', '20', '3', '4', '5', '5', '5');
INSERT INTO `data_laptop` VALUES (6, 'Acer 1', '5000000', '12', '512', 'AMD', '21', '4', '3', '4', '3', '5');
INSERT INTO `data_laptop` VALUES (7, 'Asus 2', '2996000', '4', '128', 'AMD', '5', '5', '1', '2', '3', '1');
INSERT INTO `data_laptop` VALUES (8, 'Hp 1', '2980000', '4', '128', 'AMD', '5', '5', '1', '2', '3', '1');
INSERT INTO `data_laptop` VALUES (9, 'MSI 3', '28800000', '16', '1024', 'INTEL', '12', '1', '4', '5', '5', '3');
INSERT INTO `data_laptop` VALUES (10, 'Hp 2', '6120000', '12', '512', 'INTEL', '13', '4', '3', '4', '5', '3');
INSERT INTO `data_laptop` VALUES (11, 'Hp 3', '11796000', '16', '1024', 'INTEL', '16', '4', '4', '5', '5', '5');
INSERT INTO `data_laptop` VALUES (12, 'Asus 3', '6836000', '8', '256', 'AMD', '13', '4', '2', '3', '3', '3');
INSERT INTO `data_laptop` VALUES (13, 'Asus 4', '18700000', '24', '1024', 'INTEL', '12', '2', '5', '5', '5', '3');
INSERT INTO `data_laptop` VALUES (14, 'Lenovo 3', '6500000', '12', '256', 'INTEL', '13', '4', '3', '3', '5', '3');
INSERT INTO `data_laptop` VALUES (15, 'Acer 2', '2080000', '4', '128', 'AMD', '5', '5', '1', '2', '3', '1');

SET FOREIGN_KEY_CHECKS = 1;
